package ru.csu.websockets.service.impl;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import ru.csu.websockets.service.SubscriptionService;

import java.io.IOException;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.*;

@Service
@Log4j2
public class SubscriptionServiceImpl implements SubscriptionService {

    private final ThreadPoolExecutor handlerExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);

    private final Set<WebSocketSession> sessions = Collections.synchronizedSet(new HashSet<>());

    private final ThreadPoolExecutor messageSender = new ThreadPoolExecutor(2, 10, 60, TimeUnit.SECONDS, new PriorityBlockingQueue<>(10000, PriorityRunnable.COMPARATOR));

    @Override
    public void subscribe(WebSocketSession session) {
        handlerExecutor.submit(() -> {
            sessions.add(session);
        });
        log.info("WS subscribe session {}", session.getId());
    }
    @Override
    public void unsubscribe(WebSocketSession session){
        handlerExecutor.remove(()->{
            sessions.add(session);
        });
        sessions.remove(session);
        log.info("WS unsubscribe session {}", session.getId());
    }

    @Override
    public void handleMessage(WebSocketSession session, TextMessage message) {
        handlerExecutor.submit(() -> {
            sessions.forEach(s ->messageSender.submit(new PriorityRunnable(PriorityRunnable.MID_PRIORITY, () -> {
                try {
                    if(s != session)
                        s.sendMessage(message);
                } catch (IOException e) {
                    log.error(e);
                }
            })));
        });
    }

    private static class PriorityRunnable implements Runnable {

        static int HIGHEST_PRIORITY = Integer.MIN_VALUE;
        static int LOWEST_PRIORITY = Integer.MAX_VALUE;
        static int MID_PRIORITY = 0;

        static Comparator<Runnable> COMPARATOR = (o1, o2) -> {
            if (o1 instanceof PriorityRunnable && o2 instanceof PriorityRunnable) {
                return ((PriorityRunnable) o2).priority - ((PriorityRunnable) o1).priority;
            } else if (o1 instanceof PriorityRunnable) {
                return 1;
            } else if (o2 instanceof PriorityRunnable) {
                return -1;
            }
            return 0;
        };

        int priority;
        long timestamp;
        Runnable runnable;

        public PriorityRunnable(int priority, long timestamp, Runnable runnable) {
            this.priority = priority;
            this.timestamp = timestamp;
            this.runnable = runnable;
        }

        public PriorityRunnable(int priority, Runnable runnable) {
            this(priority, Instant.now().toEpochMilli(), runnable);
        }

        @Override
        public void run() {
            this.runnable.run();
        }
    }
}
